---
layout: page
title:  About
---

My name is Victor, but in the virtual world I usually go by Phaedrus. Yes, I read Zen and the Art of Motorcycle Maintenance, and I liked it quite a bit.


You can reach me at:

- StackerNews: [https://stacker.news/phaedrus](https://stacker.news/phaedrus)
- Nostr: [e6a92d8b6c20426f78bba8510ccdc73df5122814a3bac1d553adebac67a92b27](https://nostr.band/?q=e6a92d8b6c20426f78bba8510ccdc73df5122814a3bac1d553adebac67a92b27)
- Twitter: [@bitcoinpolis](https://twitter.com/bitcoinpolis)
- Session: 05b88447ebeb5094ddc96f00215fb270a1b8e118e205190ec165cc0245ec5b906d
- E-mail: [philosopher.phaedrus@protonmail.com](mailto://philosopher.phaedrus@protonmail.com)
- Telegram: [https://t.me/p_phaedrus](https://t.me/p_phaedrus)
