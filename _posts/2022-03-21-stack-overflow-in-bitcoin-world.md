---
layout: post
title:  "Stack Overflow in Bitcoin world"
date:   2022-03-21
categories: startup product
---

Could Stack Overflow be more successful if it was build in the Bitcoin world?

### The basics

It's just like Stack Overflow, but when you like an answer you tip with real sats. And if it really saved your life, or even just a few hours of debugging, you can tip a lot!

If other people find your answers valuable, you can buy real stuff, because you are earning real sats, not some points and badges.
Where can this go?

I think the current lack of incentives limits the potential of Stack Overflow. When you integrate "real money" incentives, the platform will be able to grow much further, and provide more value.

People could post bounties: promise how many sats they will pay to whoever answers their question. These sats go into an escrow and eventually get released to the author of the best answer. Yes, you will probably need some smart-ish contract to enforce this, maybe other users approving which answer solved the original question, if the author doesn't want to accept any answer.

It could grow into an education platform: people can offer pair programming sessions to fully explain complicated answers.

It could even grow into a job platform: you can hire someone to fully solve a task for you, and provide fully baked solution.


### Where to start?

The basic functionality of asking questions and submitting answers is not that hard to build, but accumulating enough content for people to use this new platform, that's the tricky part. Thus you need to find a very focused community, and start small. I think focusing on Bitcoin/Lightning is your best bet, because those people won't have trouble setting things up.

The onboarding must be as easy as possible. No registrations, no app downloads. Just open the website and you can immediately post question or answer. As much as tell everyone to hold their keys and avoid custodial solutions, I think making the experience seamless will be key and so it's better to start with custodial Lightning wallet that the every user can top up right when they try to tip first answer (and tipping future answers is just a tap away), and they can even start stacking sats without having their own wallet and only withdraw once they accumulate enough.

If you are interested in building something like this, [reach out](/about).
