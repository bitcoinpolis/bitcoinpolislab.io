---
layout: post
title:  "Private Finance Tracker"
date:   2022-04-29
categories: startup product
---

When I was little, my parents required me to keep track of all my spendings, or else they wouldn't give me any weekly allowance. I used to think it was a tedious and pointless exercise, but now I see how it helped me plan, prioritize, and -- most importantly -- judge prices (whether something was worth the money). Yes, it also taught me how to make up fake expenses so that the math added up at the end of the week.

Today, almost thirty years later, I still keep track of expenses to make sure our family survives and has money for the things we find important. Now that [Jack Mallers announced](https://www.youtube.com/watch?v=dD2-T7TX2rk) that most stores will be accepting Lightning, I expect to be moving most of my daily spending over to Lightning, and I would love to be able to track my expenses across different wallets, but of course I don't want other people to see it. Something like [Mint](https://mint.intuit.com/), but with privacy. You don't want some evil company to know what you buy and use it to control you.

Bitcoin is about financial sovereignity, and I think this could eventually grow into a financial education platform. I would certainly love my kids to learn how to save up for bigger things and spend their sats according to their long-term goals.

### How could this work?

The wallet needs to emit some sort of notification for each transaction. Then we need an easy UI that listens to this notification to quickly label each transaction (gas, groceries, kids, hidden, etc.), and a UI for analyzing your spending habits and learning from it. Later, you can build all kinds of features like alerts, budgeting, or coaching based on the user's financial goals.

Eventually, this needs to be supported by most wallets, so there should be some BIP/BOLT standard, and the labeling should be done outside of the wallet so that it's a single app, but the first version could be all hacked inside one of the existing open source wallets.

If you are interested in building something like this, [reach out](/about).
