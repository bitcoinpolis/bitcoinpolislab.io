---
layout: post
title:  "Listening to early customers"
date:   2022-02-17
categories: startup product
---

Startups often try to grow the number of customers as fast as possible, and I think it’s a mistake.

Listening to your customers is critical for building successful product. When you have just three key customers, you can listen and understand them much better than when you have 3,000. When you reach hundreds of customers you need to start relying on metrics and use statistics to understand the feedback, which is much harder to do right.

That’s why you really want to focus on the right first customers and get the most feedback out of them. They are basically helping you build the product, so don’t waste that opportunity! Pay attention to how they are using the product, talk to them a lot, and figure out what problem you are really solving.

During this early stage, I like to keep an eye on how much the top customers spend (either in money or time), and wanna see that grow. That’s quicker metric for developing the right product than the number of active users which takes much longer to change.

Yes, it’s much better to have just a couple of customers who can’t live without your product, instead of thousands of registered or “active” customers who don’t really care.
